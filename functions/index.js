const functions = require("firebase-functions");
const express = require("express");
const admin = require("firebase-admin");
const bodyParser = require("body-parser");

admin.initializeApp();

const MAX_PLAYERS = 2;

const app = express();
app.use(bodyParser.json());

const db = admin.database();

function sendJson(res, obj, code = 200) {
  res.json(obj);
  res.end();
}

const State = {
  PROLOGUE: "prologue",
  INPROGRESS: "inprogress",
  FINISHED: "finished"
};

app.post("/v1/room/join", async (req, res) => {
  console.log("/v1/room/join: " + JSON.stringify(res.body));

  const roomsRef = db.ref("/rooms");
  const roomsSnapshot = await roomsRef.once("value");
  let roomSnapshot = null;

  roomsSnapshot.forEach(item => {
    if (item.val()["players"].length === 1) {
      roomSnapshot = item;
    }
  });

  newRoom = !roomSnapshot;
  const roomKey = roomSnapshot ? roomSnapshot.key : (await roomsRef.push()).key;
  const player = newRoom ? 0 : 1;
  const playerPath = `players/${player}`;

  const updates = {
    "active_player": 0,
    "last_player_hit": false,
    "state": player === MAX_PLAYERS - 1 ? State.INPROGRESS : State.PROLOGUE
  };
  updates[playerPath] = 0;

  await roomsRef.child(roomKey).update(updates);
  return sendJson(res, { player: player, room: roomKey });
});

app.post("/v1/room/:room/turn", async (req, res) => {
  const roomRef = db.ref("/rooms").child(req.params.room);
  const activePlayer = (await roomRef
    .child("active_player")
    .once("value")).val();

  if (activePlayer !== req.body.player) {
    return sendJson(res, { error: "Invalid player" }, 400);
  }

  await db
    .ref("/rooms")
    .child(req.params.room)
    .child("turns")
    .push({ ...req.body, ts: Date.now() });

  if (req.body.hit) {
    const playerRef = db
      .ref("/rooms")
      .child(req.params.room)
      .child("players")
      .child(req.body.player);
    const oldScores = (await playerRef.once("value")).val();
    await playerRef.set(oldScores + 1);
  }

  const nextPlayer = (activePlayer + 1) % MAX_PLAYERS;
  console.log(`${req.params.room}: active_player: ${activePlayer}, next_player: ${nextPlayer}`);

  await db
    .ref("/rooms")
    .child(req.params.room)
    .update({
      last_player_hit: req.body.hit,
      active_player: nextPlayer
    });

  res.end();
});

app.get("/v1/room/:room", async (req, res) => {
  const state = await db
    .ref("/rooms")
    .child(req.params.room)
    .once("value");

  res.send(JSON.stringify(state.val()));
  res.end();
});

exports.api = functions.https.onRequest(app);
