const Scene = require('Scene');
const Diagnostics = require('Diagnostics');

const Animation = require('Animation');
const Reactive = require('Reactive');
const TouchGestures = require('TouchGestures');
const Networking = require('Networking');
const Time = require("Time");

//objects
const cup_ctrl = Scene.root.find('cupController')
const planeTracker = Scene.root.find('planeTracker0');
const welcomeText = Scene.root.find('welcomeText');
const textBackground = Scene.root.find('textBackground');

const Camera = Scene.root.find('Camera');

const powerSlider = Scene.root.find('powerSlider');
const powerSelector = Scene.root.find('powerSelector');
const ball = Scene.root.find('ball');
const scoresLabel = Scene.root.find('scoresLabel');
const statusLabel = Scene.root.find('statusLabel');

// const enemy = Scene.root.find('enemy');

var GameStates = {
   Pregame : 0,
   MyTurn : 1,
   EnemyTurn : 2,
   Result : 3
};

var BallStates = {
   Follow : 0,
   NotFollow : 1
}

//globals
var ballState;
var base = {
   // sliderYBase: 0,
   sliderHeight: 0,
   g_accel: 9.8,
}
var ctx = {
   room: null,
   player: null,
   uiState: null,
   gameState: {},
   sliderTransformY: 0,
   sliderYOffset: 0,
   objects: {
      ball: {
         x: 0,
         y: 0
      }
   }
};
var subscriptions = {};



var State = {
   PROLOGUE: "prologue",
   INPROGRESS: "inprogress",
   FINISHED: "finished"
};

// Firebase Functions endpoint const ROOT_API_ENDPOINT = "https://us-central1-beerpong-7ff0e.cloudfunctions.net/api/v1";
// Express.js endpoint
const ROOT_API_ENDPOINT = "https://sodium.tk/arpong/v1";
var api = {};

api.request = function(endpoint, request) {
   const url = ROOT_API_ENDPOINT + '/' + endpoint;
   Diagnostics.log("api(): Doing a HTTP request " + request.method + " " + url);

   return Networking.fetch(url, request).then(function(result) {
      // The result object contains information about the request,
      // such as status code and response body.
      if ((result.status >= 200) && (result.status < 300)) {
         // If the request was successful, we'll chain the JSON forward
         // Diagnostics.log("api(): request completed: " + JSON.stringify(result.json()));
         return result.json();
      }
      Diagnostics.log("api(): http status code is " + result.status);
      throw new Error(endpoint + " HTTP status code " + result.status);
   }).then(function(json) {
      Diagnostics.log("api(): request completed: " + JSON.stringify(json));
      return json;
   });
}

api.GET = function(endpoint) {
   return api.request(endpoint, {method: "GET"});
}

api.POST = function(endpoint, obj) {
   return api.request(endpoint, {
      method: "POST",
      headers: {"Content-Type": "application/json"},
      body: obj && JSON.stringify(obj)
   });
}

api.join = function() {
   return api.POST("room/join", {});
}

api.turn = function(room, player, hit) {
   return api.POST("room/" + room + "/turn", {player, hit});
}

api.state = function(room) {
   return api.GET("room/" + room);
}

api.rooms = function() {
   return api.GET("room/");
}

var utils = {
   normalizePower: function(value) {
      return this.normalize(value, 0, base.sliderHeight, 3, 6);
   },
   normalize: function (value, oldMin, oldMax, min, max) {
      var normalized = (max - min)/(oldMax - oldMin) * (value - oldMax) + max;
      return normalized;
   }
}


function updateState(state) {
   if (state === ctx.uiState)
      return;

   ctx.uiState = state;
   Diagnostics.log("updateState: " + state);

   switch(state) {

      case GameStates.Pregame:
         powerSelector.hidden = true;
         powerSlider.hidden = true;
         ball.hidden = true;
         break;
      case GameStates.MyTurn:
         powerSelector.hidden = false;
         powerSelector.transform.y = 0;
         powerSlider.hidden = false;
         ball.hidden = false;
         setBallState(BallStates.Follow);

         welcomeText.hidden = true;
         textBackground.hidden = true;

         subscriptions.myTurnOnPan = TouchGestures.onPan(powerSelector).subscribe(function (gesture) {
               var translation = gesture.translation.y;

               powerSelector.transform.y = Reactive.neg(Reactive.max(-180, Reactive.min(180, translation)));
               subscriptions.panGestureState = gesture.state.monitor().subscribe((state) => {
                  Diagnostics.log("ballState follow: " + ballState);
                  if (state.newValue === TouchGestures.Gesture.State.ENDED && ballState === BallStates.Follow) {
                        var throwPower = utils.normalizePower(ctx.sliderTransformY);
                        Diagnostics.log("transl:" + ctx.sliderTransformY + " normalzed: " + throwPower);

                        throwBall(throwPower);
                     // });
                  }
               });

            // }
         });

         break;
      case GameStates.EnemyTurn:
         powerSelector.hidden = true;
         powerSlider.hidden = true;
         ball.hidden = true;

         powerSelector.transform.y = powerSlider.bounds.height;
         subscriptions.onPanTrack.unsubscribe();

         ball.hidden = true;
         ball.transform.x = Camera.worldTransform.x;
         ball.transform.y = Camera.worldTransform.y;
         ball.transform.z = Camera.worldTransform.z;

         break;
      case GameStates.Result:
         powerSelector.hidden = true;
         powerSlider.hidden = true;
         ball.hidden = true;
         break;
   }
}

function longTick() {
   // if (ctx.gameState.active_player !== ctx.player || ctx.gameState.state === State.PROLOGUE)
   {
      api.state(ctx.room).then(function(state) {
         Diagnostics.log("Updating game state " + JSON.stringify(state));
         ctx.gameState = state;
         if (ctx.gameState.state === State.PROLOGUE) {
            updateState(GameStates.Pregame);
         }
         else if (ctx.gameState.state === State.INPROGRESS) {
            if (ctx.gameState.active_player === ctx.player) {
               updateState(GameStates.MyTurn);
            } else {
               updateState(GameStates.EnemyTurn);
            }

            scoresLabel.text = state.players[0] + ':' + state.players[1];
         }
         statusLabel.text = state.state;
      });
   }
}

function setBallState(state) {
   Diagnostics.log("setBallState: " + state);
   ballState = state;
   if (ballState === BallStates.NotFollow) {
      ball.transform.x = 0;
      ball.transform.y = 0;
      ball.transform.z = 0;

      ball.transform.rotationX = 0;
      ball.transform.rotationY = 0;
      ball.transform.rotationZ = 0;

   }
   if (ballState === BallStates.Follow) {
      ball.transform.x = Camera.worldTransform.x; //Reactive.add(Camera.worldTransform.x, 0.1);
      ball.transform.y = Camera.worldTransform.y;
      ball.transform.z = Camera.worldTransform.z;

      ball.transform.rotationX = Camera.worldTransform.rotationX;
      ball.transform.rotationY = Camera.worldTransform.rotationY;
      ball.transform.rotationZ = Camera.worldTransform.rotationZ;
   }
}

function checkHit() {

   // cup_ctrl.boundingBox
}


var throwAngle = 70 * Math.PI/180;
var ballTime = 0;
var timeDiff = 0.01;


function updateBall(power) {
   var radius = 0.01;
   ballTime += timeDiff;
   var z = -power * ballTime * Math.cos(throwAngle);
   var y = power * ballTime * Math.sin(throwAngle) - 1/2*base.g_accel * ballTime * ballTime;
   ball.transform.z = z;
   ball.transform.y = y;


      //in cup height
      if (ctx.ballY < ctx.cupY) {
         var ballCupOffsetX = ctx.ballX - ctx.cupX;
         var ballCupOffsetZ = ctx.ballZ - ctx.cupZ;
         if (radius * radius > Math.pow(z, 2) + Math.pow(x,2)) {
            Diagnostics.log("HIT");
            api.turn(ctx.room, ctx.player, true);
            updateState(GameStates.EnemyTurn);
            return;
         }

      }

   if (ctx.ballY < -0.14) {
      Diagnostics.log("MISSED");

      api.turn(ctx.room, ctx.player, false);

      updateState(GameStates.EnemyTurn);
      return;
   }



   // checkHit();
   Time.setTimeout(()=>updateBall(power), 25);
}
var powerMod = 1.2;

var ballInsideX = false;
function throwBall(power) {
   Diagnostics.log("throwingBall");
   power*=powerMod;


   setBallState(BallStates.NotFollow);
   subscriptions.ballY = ball.worldTransform.y.monitor().subscribe((y) => {
      ctx.ballY = y.newValue;
   });

   subscriptions.ballX = ball.worldTransform.x.monitor().subscribe((x) => {
      ctx.ballX = x.newValue;
   });
   subscriptions.ballZ = ball.worldTransform.z.monitor().subscribe((z) => {
      ctx.ballZ = z.newValue;
   });

   subscriptions.cupX = cup_ctrl.worldTransform.x.monitor().subscribe((x) => {
      ctx.cupX = x.newValue;
   });
   subscriptions.cupY = cup_ctrl.worldTransform.y.monitor().subscribe((y) => {
      ctx.cupY = y.newValue;
   });
   subscriptions.cupZ = cup_ctrl.worldTransform.z.monitor().subscribe((z) => {
      ctx.cupZ = z.newValue;
   });

   updateBall(power);



}

function init() {
   Diagnostics.log("init");
   initSubscriptions();
   return api.join().then(function(joinRes) {
      ctx.room = joinRes.room;
      ctx.player = joinRes.player;
      return api.state(ctx.room);
   }).then(function(state) {
      Diagnostics.log("init(): update game state");
      // ctx.gameState = state;
      longTick();
      Time.setInterval(longTick, 2000);
   });
}

function initSubscriptions() {
   Diagnostics.log("initSubscriptions");
   subscriptions.onTapTrack = TouchGestures.onTap().subscribe(function(gesture) {
      planeTracker.trackPoint(gesture.location);
   });

   subscriptions.onPanTrack = TouchGestures.onPan(planeTracker).subscribe(function(gesture) {
      planeTracker.trackPoint(gesture.location, gesture.state);
   });

   subscriptions.onPinchTrack = TouchGestures.onPinch().subscribe(function(gesture) {
      var lastScaleX = cup_ctrl.transform.scaleX.lastValue;
      cup_ctrl.transform.scaleX = Reactive.mul(lastScaleX, gesture.scale);

      var lastScaleY = cup_ctrl.transform.scaleY.lastValue;
      cup_ctrl.transform.scaleY = Reactive.mul(lastScaleY, gesture.scale);

      var lastScaleZ = cup_ctrl.transform.scaleZ.lastValue;
      cup_ctrl.transform.scaleZ = Reactive.mul(lastScaleZ, gesture.scale);
   });

   subscriptions.onRotateTrack = TouchGestures.onRotate(cup_ctrl).subscribe(function(gesture) {
     var lastRotationY = cup_ctrl.transform.rotationY.lastValue;
     cup_ctrl.transform.rotationY = Reactive.add(lastRotationY, Reactive.mul(-1, gesture.rotation));
   });

   subscriptions.sliderHeight = powerSlider.bounds.height.monitor().take(1).subscribe((val)=>{
      base.sliderHeight = val.newValue;
      Diagnostics.log("sliderHeight: "+ val.newValue);

      subscriptions.sliderHeight.unsubscribe();
   });

   subscriptions.sliderOffset = powerSelector.bounds.y.monitor().subscribe((y) => {
      ctx.sliderYOffset = y.newValue;
      Diagnostics.log("sliderYOffset: "+ ctx.sliderYOffset);
   });

   subscriptions.sliderTransformY = powerSelector.transform.y.monitor().subscribe((y) => {
      ctx.sliderTransformY = -y.newValue;
   });
}
// initSubscriptions();
// updateState(GameStates.MyTurn);
// throwBall(50);

init().then(function() {
  Diagnostics.log("main: ok");
}).catch(function(error) {
   Diagnostics.log("main: " + error);
});