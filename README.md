This is [Hackjunction](https://en.wikipedia.org/wiki/Junction_(hackathon)) 2018 project.
Beerpong includes two parts:
- Facebook Spark Studio Application
- REST API powered by the Node.js lamda-functions on Google Firebase Serverless. For debugging Node.js Express.js was used.

# UX Flows
![](demo/beerpong.gif)

## Player 0 UX flow
* Generate session id using RandomModule.random() https://developers.facebook.com/docs/ar-studio/reference/classes/randommodule
* Place an glass on the table.
* Send a room name to the backend
* Wait for the another player
* Show the ball, velocity controller
* Player 0 choose an velocity and throw the ball
* Send api/v1/:room/turn request
* Wait for the Player 1 turn
* Display the Player 1 turn (hit or not hit)
* If the Player 1 hasn't hit, change the color to blue.

## Player 2 UX flow
* Get list of the rooms, join to the random room.
* Wait the Player 0 turn
* Displaye the Player 0 turn
* If the Player 1 hasn't hit, change the color to blue.
* Show the ball, velocity controller
* Player 1 choose an velocity and throw the ball
* Send api/v1/:room/turn request



# API Endpoints
Root path is https://us-central1-beerpong-7ff0e.cloudfunctions.net


| Description                               |                                                                   |
|:------------------------------------------|------------------------------------------------------------------:|
|Join a room                                | POST api/v1/:room/join {"player": "player_id"}                    |
|Do a turn                                  | POST api/v1/:room/turn {"hit": boolean, "player": "player_id" }   |
|Get list of the available rooms            | GET api/v1/room                                                   |
|Get game state                             | GET api/v1/:room                                                  |
